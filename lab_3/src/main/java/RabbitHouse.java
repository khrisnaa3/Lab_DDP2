import java.util.Scanner;

public class RabbitHouse {
	public static void main (String[] args) {
		Scanner input = new Scanner(System.in);

		String type = input.next();
		String nama = input.next();

		int banyakHuruf = nama.length();

		if (type.equals("normal") && banyakHuruf <= 10){
			System.out.println(anak(banyakHuruf,1));
		}else{
			System.out.println("Pastikan tipe kelinci benar dan nama kelinci tidak lebih dari 10 huruf");
		}
	}
	
	public static int anak(int kata, int jumlah) {
		if (kata <= 1) {
			return 1;
		}
		else {
			return kata * jumlah + (anak((kata - 1), kata * jumlah));
		}
	}
}