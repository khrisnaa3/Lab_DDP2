package xoxo;

import java.awt.*;
import java.awt.event.ActionListener;

import javax.swing.*;

/**
 * This class handles most of the GUI construction.
 * 
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 * @author Stefanus Khrisna
 */
public class XoxoView {
    
    /**
     * A field that used to be the input of the
     * message that wants to be encrypted/decrypted.
     */
    private JTextField messageField;

    /**
     * A field that used to be the input of the key string.
     * It is a Kiss Key if it is used as the encryption.
     * It is a Hug Key if it is used as the decryption.
     */
    private JTextField keyField;

    
    /**
     * A field to be the input of the seed.
     */
    private JTextField seedField;

    /**
     * A field that used to display any log information such
     * as you click the button, an output file succesfully
     * created, etc.
     */
    private JTextArea logField; 

    /**
     * A button that when it is clicked, it encrypts the message.
     */
    private JButton encryptButton;

    /**
     * A button that when it is clicked, it decrpyts the message.
     */
    private JButton decryptButton;

    private JFrame frame;

    /**
     * Class constructor that initiates the GUI.
     */
    public XoxoView() {
        this.initGui();
    }

    /**
     * Constructs the GUI.
     */
    private void initGui() {
        frame = new JFrame("Xoxo Encryptor & Decryptor");
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setLayout(new BorderLayout());
        JPanel topPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        topPanel.setBackground(Color.black);
        JPanel centerPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        centerPanel.setBackground(Color.black);
        JPanel bottomPanel = new JPanel(new GridLayout(1,1));
        bottomPanel.setBackground(Color.black);

        JPanel textPanel = new JPanel(new GridLayout(3,1));
        JPanel messagePanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        messagePanel.setBackground(Color.black);
        JPanel keyPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        keyPanel.setBackground(Color.black);
        JPanel seedPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        seedPanel.setBackground(Color.black);

        JPanel buttonPanel = new JPanel(new GridLayout(3,1));
        buttonPanel.setBackground(Color.black);
        JLabel commandText = new JLabel("Please Choose The Command");
        commandText.setForeground(Color.green);
        JPanel encryptPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        encryptPanel.setBackground(Color.black);
        JPanel decryptPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        decryptPanel.setBackground(Color.black);

        JLabel messageLabel = new JLabel("Message   :");
        messageLabel.setForeground(Color.green);
        JLabel keyLabel = new JLabel("Key       :");
        keyLabel.setForeground(Color.green);
        JLabel seedLabel = new JLabel("Seed     :");
        seedLabel.setForeground(Color.green);

        messageField = new JTextField();
        messageField.setBackground(Color.black);
        messageField.setCaretColor(Color.red);
        messageField.setForeground(Color.yellow);
        messageField.setPreferredSize(new Dimension(300,25));
        keyField = new JTextField();
        keyField.setBackground(Color.black);
        keyField.setCaretColor(Color.red);
        keyField.setForeground(Color.yellow);
        keyField.setPreferredSize(new Dimension(300,25));
        seedField = new JTextField();
        seedField.setBackground(Color.black);
        seedField.setCaretColor(Color.red);
        seedField.setForeground(Color.yellow);
        seedField.setPreferredSize(new Dimension(300,25));

        encryptButton = new JButton("Encrypt");
        encryptButton.setForeground(Color.red);
        encryptButton.setPreferredSize(new Dimension(90,30));
        decryptButton = new JButton("Decrypt");
        decryptButton.setForeground(Color.red);
        decryptButton.setPreferredSize(new Dimension(90,30));

        logField = new JTextArea("---------------------------------------------------------------------------------------------------" +
                "\n===Log===\n\n");
        logField.setBackground(Color.black);
        logField.setCaretColor(Color.red);
        logField.setForeground(Color.yellow);
        logField.setPreferredSize(new Dimension(400,300));

        messagePanel.add(messageLabel);
        messagePanel.add(messageField);
        keyPanel.add(keyLabel);
        keyPanel.add(keyField);
        seedPanel.add(seedLabel);
        seedPanel.add(seedField);

        textPanel.add(messagePanel);
        textPanel.add(keyPanel);
        textPanel.add(seedPanel);

        encryptPanel.add(encryptButton);
        decryptPanel.add(decryptButton);

        buttonPanel.add(commandText);
        buttonPanel.add(encryptPanel);
        buttonPanel.add(decryptPanel);


        topPanel.add(textPanel);
        centerPanel.add(buttonPanel);
        bottomPanel.add(logField);

        frame.add(topPanel, BorderLayout.NORTH);
        frame.add(centerPanel, BorderLayout.CENTER);
        frame.add(bottomPanel, BorderLayout.SOUTH);
        frame.pack();
        frame.setVisible(true);
    }

    /**
     * Gets the message from the message field.
     * 
     * @return The input message string.
     */
    public String getMessageText() {
        return messageField.getText();
    }

    /**
     * Gets the key text from the key field.
     * 
     * @return The input key string.
     */
    public String getKeyText() {
        return keyField.getText();
    }

    /**
     * Gets the seed text from the key field.
     * 
     * @return The input key string.
     */
    public String getSeedText() {
        return seedField.getText();
    }

    /**
     * Appends a log message to the log field.
     *
     * @param log The log message that wants to be
     *            appended to the log field.
     */
    public void appendLog(String log) {
        logField.append(log + '\n');
    }

    /**
     * Sets an ActionListener object that contains
     * the logic to encrypt the message.
     * 
     * @param listener An ActionListener that has the logic
     *                 to encrypt a message.
     */
    public void setEncryptFunction(ActionListener listener) {
        encryptButton.addActionListener(listener);
    }
    
    /**
     * Sets an ActionListener object that contains
     * the logic to decrypt the message.
     * 
     * @param listener An ActionListener that has the logic
     *                 to decrypt a message.
     */
    public void setDecryptFunction(ActionListener listener) {
        decryptButton.addActionListener(listener);
    }

    public void setWarning(String warning) {
        JOptionPane.showMessageDialog(frame, warning);
    }
}