package xoxo;

import xoxo.crypto.XoxoDecryption;
import xoxo.crypto.XoxoEncryption;
import xoxo.exceptions.*;
import xoxo.util.XoxoMessage;

import static xoxo.key.HugKey.DEFAULT_SEED;

/**
 * This class controls all the business
 * process and logic behind the program.
 *
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 * @author Stefanus Khrisna
 */
public class XoxoController {

    /**
     * The GUI object that can be used to get
     * and show the data from and to users.
     */
    private XoxoView gui;

    /**
     * Class constructor given the GUI object.
     */
    public XoxoController(XoxoView gui) {
        this.gui = gui;
    }

    /**
     * Main method that runs all the business process.
     */
    public void run() {
        gui.setEncryptFunction(e -> encryptMessage());
        gui.setDecryptFunction(e -> decryptMessage());
    }

    /**
     * Method to encrypt the message.
     */
    public void encryptMessage() {
        String messageText = gui.getMessageText();
        String kissKey = gui.getKeyText();
        String seed = gui.getSeedText();
        XoxoEncryption encryptMsg = null;
        try {
            encryptMsg = new XoxoEncryption(kissKey);
        } catch (KeyTooLongException e) {
            gui.setWarning("Key length must not exceed 28");
            return;
        } catch (InvalidCharacterException e) {
            gui.setWarning("Key can contain only A-Z, a-z, and @");
            return;
        }
        XoxoMessage message;
        try {
            if (seed.equals("") || Integer.parseInt(seed) == 18 || seed.equals("DEFAULT_SEED")) {
                message = encryptMsg.encrypt(messageText);
            } else {
                message = encryptMsg.encrypt(messageText, Integer.parseInt(seed));
            }
        } catch (RangeExceededException e) {
            gui.setWarning("Seed must between 0 and 36 (inclusive)");
            return;
        } catch (NumberFormatException e) {
            gui.setWarning("Seed must be a number!");
            return;
        } catch (SizeTooBigException e) {
            gui.setWarning("Message size too big!");
            return;
        }
        printLog(message.getEncryptedMessage());
    }

    /**
     * Method to decrypt the message.
     */
    public void decryptMessage() {
        String hugKey = gui.getKeyText();
        String encryptedMessage = gui.getMessageText();
        XoxoDecryption decryptMsg = new XoxoDecryption(hugKey);
        int seed;
        if (gui.getSeedText().equals("") || Integer.parseInt(gui.getSeedText()) == 18 || gui.getSeedText().equals("DEFAULT_SEED")) {
            seed = DEFAULT_SEED;
        } else {
            seed = Integer.parseInt(gui.getSeedText());
        }
        String decryptedMessage = decryptMsg.decrypt(encryptedMessage, seed);
        printLog(decryptedMessage);
    }

    public void printLog(String message) {
        gui.appendLog(message);
    }
}