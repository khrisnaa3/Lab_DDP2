import character.*;
import java.util.ArrayList;

public class Game{
    ArrayList<Player> player = new ArrayList<Player>();
    

    /**
    * Fungsi untuk mencari karakter
    * @param String name nama karakter yang ingin dicari
    * @return Player chara object karakter yang dicari, return null apabila tidak ditemukan
    */
    public Player find(String name){
        for (Player pemain : player) {
            if (pemain.getName().equals(name)) return pemain;
        }
        return null;
    }

    /**
    * fungsi untuk menambahkan karakter ke dalam game
    * @param String chara nama karakter yang ingin ditambahkan
    * @param String tipe tipe dari karakter yang ingin ditambahkan terdiri dari monster, magician dan human
    * @param int hp hp dari karakter yang ingin ditambahkan
    * @return String result hasil keluaran dari penambahan karakter contoh "Sudah ada karakter bernama chara" atau "chara ditambah ke game"
    */
    public String add(String chara, String tipe, int hp){
        if (find(chara) != null) {
            return "Sudah ada karakter bernama " + chara;
        }
        if (tipe.equals("Human")) {
            player.add(new Human(chara, hp));
        } else if (tipe.equals("Monster")) {
            player.add(new Monster(chara, hp));
        } else if (tipe.equals("Magician")){
            player.add(new Magician(chara, hp));
        }
        return chara + " ditambah ke game";
    }

    /**
    * fungsi untuk menambahkan karakter dengan tambahan teriakan roar, roar hanya bisa dilakukan oleh monster
    * @param String chara nama karakter yang ingin ditambahkan
    * @param String tipe tipe dari karakter yang ingin ditambahkan terdiri dari monster, magician dan human
    * @param int hp hp dari karakter yang ingin ditambahkan
    * @param String roar teriakan dari karakter
    * @return String result hasil keluaran dari penambahan karakter contoh "Sudah ada karakter bernama chara" atau "chara ditambah ke game"
    */
    public String add(String chara, String tipe, int hp, String roar){
        if (find(chara) != null) {
            return "Sudah ada karakter bernama " + chara;
        }
        player.add(new Monster(chara, hp, roar));
        return chara + " ditambah ke game";
    }

    /**
    * fungsi untuk menghapus character dari game
    * @param String chara character yang ingin dihapus
    * @return String result hasil keluaran dari game
    */
    public String remove(String chara){
        if (find(chara) != null) {
            player.remove(find(chara));
            return chara + " dihapus dari game";
        }
        return "Tidak ada " + chara;
    }


    /**
    * fungsi untuk menampilkan status character dari game
    * @param String chara character yang ingin ditampilkan statusnya
    * @return String result hasil keluaran dari game
    */
    public String status(String chara){
        if (find(chara) == null) {
            return chara + " tidak ada";
        }

        String mati = "Masih hidup";
        String makanan = "Belum memakan siapa siapa";
        int jumlahDiet = find(chara).getDiet().size();
        if (jumlahDiet > 0) {
            makanan = "Memakan ";
            for (Player pemain : find(chara).getDiet()) {
                makanan += pemain.getTipe() + " " + pemain.getName() + ",";
            }
        }
        if (find(chara).isMati()) {
            mati = "Sudah meninggal dunia dengan damai";
        }
        String tipe = find(chara).getTipe();
        String nama = find(chara).getName();
        int HP = find(chara).getHp();
        return String.format("%s %s\nHP: %d\n%s\n%s", tipe, nama, HP, mati, makanan);
    }

    /**
    * fungsi untuk menampilkan semua status dari character yang berada di dalam game
    * @return String result nama dari semua character, format sesuai dengan deskripsi soal atau contoh output
    */

    public String status(){
        String kembali = "";
        for (Player pemain : player) {
            kembali += status(pemain.getName()) + "\n";
        }
        if (player.size() == 0) {
            return "Tidak ada pemain";
        }
        return kembali;
    }

    /**
    * fungsi untuk menampilkan character-character yang dimakan oleh chara
    * @param String chara Player yang ingin ditampilkan seluruh history player yang dimakan
    * @return String result hasil dari karakter yang dimakan oleh chara
    */
    public String diet(String chara){
        String hasil = "";
        ArrayList<Player> diet = find(chara).getDiet();

        if (find(chara) == null) {
            return chara + " tidak ada";
        } else if (diet.size() == 0) {
            return "Belum ada yang termakan";
        }
        for (Player pemain : diet) {
            hasil += pemain.getTipe() + " " + pemain.getName() + ",";
        }
        return hasil;
    }

    /**
    * fungsi helper untuk memberikan list character yang dimakan dalam satu game
    * @return String result hasil dari karakter yang dimakan dalam 1 game
    */
    public String diet(){
        String hasil = "";

        for (Player pemain : player) {
            if (!diet(pemain.getName()).equals("Belum ada yang termakan")) {
                hasil += diet(pemain.getName());
            }
        }
        if (hasil.length() <= 0) {
            return "Belum ada yang termakan";
        }
        return hasil;
    }


    /**
    * fungsi untuk menampilkan hasil dari me vs enemyName
    * @param String meName nama dari character yang sedang dimainkan
    * @param String enemyName nama dari character yang akan di serang
    * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
    */
    public String attack(String meName, String enemyName){
        if (find(meName) == null || find(enemyName) == null) {
            return "Tidak ada " + meName + " atau " + enemyName;
        } else if (find(meName).isMati()) {
            return meName + " tidak bisa menyerang " + enemyName;
        }
        find(meName).attack(find(enemyName));
        return "Nyawa " + enemyName + " " + find(enemyName).getHp();
    }


    /**
    * fungsi untuk menampilkan hasil dari me vs enemyName. Method ini hanya boleh dilakukan oleh magician
    * @param String meName nama dari character yang sedang dimainkan
    * @param String enemyName nama dari character yang akan di bakar
    * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
    */
    public String burn(String meName, String enemyName){
        if (find(meName) == null) {
            return "Tidak ada " + meName + " atau " + enemyName;
        } else if (find(meName).isMati()) {
            return meName + " tidak bisa membakar " + enemyName;
        }
        find(meName).burn(find(enemyName));
        if (find(enemyName).isMati()) {
            return "Nyawa " + enemyName + " " + find(enemyName).getHp() + "\n dan matang";
        }
        return "Nyawa " + enemyName + " " + find(enemyName).getHp();
    }

    /**
    * fungsi untuk menampilkan hasil dari me vs enemyName. enemy hanya bisa dimakan sesuai dengan deskripsi yang ada di soal
    * @param String meName nama dari character yang sedang dimainkan
    * @param String enemyName nama dari character yang akan di makan
    * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
    */
    public String eat(String meName, String enemyName){
        if (find(meName) == null || find(enemyName) == null) {
            return "Tidak ada " + meName + " atau " + enemyName;
        } else if (find(meName).isMati() || !find(meName).canEat(find(enemyName))) {
            return meName + " tidak bisa memakan " + enemyName;
        }
        find(meName).eat(find(enemyName));
        remove(enemyName);
        return String.format("%s memakan %s\nNyawa %s kini %d", meName, enemyName, meName, find(meName).getHp());
    }

    /**
    * fungsi untuk berteriak. Hanya dapat dilakukan oleh monster.
    * @param String meName nama dari character yang akan berteriak
    * @return String result kembalian dari teriakan monster, format sesuai deskripsi soal
    */
    public String roar(String meName){
        if (find(meName) == null) {
            return "Tidak ada " + meName;
        } else if (find(meName) instanceof Monster) {
            return ((Monster) find(meName)).roar();
        } else if (find(meName).isMati()) {
            return meName + " tidak bisa roar";
        } else {
            return meName + " tidak bisa berteriak";
        }
    }
}