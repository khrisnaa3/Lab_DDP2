package character;


public class Magician extends Human {
    private static String tipe = "Magician";

    public Magician (String name, int hp) {
        super(name, hp);
    }

    @Override
    public void attacked() {
        this.hp -= 20;
    }

    @Override
    public void burned() {
        this.hp -= 20;
    }

    @Override
    public String getTipe() {
        return this.tipe = "Magician";
    }
}

//  write Magician Class here

