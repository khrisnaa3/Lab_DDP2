package character;


import java.util.ArrayList;

public class Player {
    protected String name;
    protected int hp;
    protected boolean matang = false;
    protected boolean mati = false;
    protected ArrayList<Player> diet = new ArrayList<Player>();
    protected String tipe;


    public Player(String name, int hp) {
        this.name = name;
        this.hp = hp;
        if (hp <= 0) mati = true;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public boolean isMatang() {
        return matang;
    }

    public void setMatang(boolean matang) {
        this.matang = matang;
    }

    public boolean isMati() {
        return mati;
    }

    public void setMati(boolean mati) {
        this.mati = mati;
    }

    public ArrayList<Player> getDiet() {
        return diet;
    }

    public void attacked() {
        this.hp -= 10;
        if (hp <= 0) {
            hp = 0;
            mati = true;
        }
    }

    public void attack(Player target) {
        if (mati) {
            return;
        }
        target.attacked();
    }

    public void burn(Player target) {
        if (mati) {
            return;
        }
        target.burned();
    }

    public void burned() {
        this.hp -= 10;
        if (hp <= 0) {
            hp = 0;
            mati = true;
            matang = true;
        }
    }

    public void eat(Player target) {
        this.hp += 15;
        this.diet.add(target);
    }

    public boolean canEat(Player target) {
        return true;
    }

    public String getTipe() {
        return tipe = "Player";
    }
}


//  write Player Class here

