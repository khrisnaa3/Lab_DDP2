package character;


public class Monster extends Player {
    private String roar = "AAAAAAaaaAAAAAaaaAAAAAA";
    private boolean matang = false;

    public Monster (String name, int hp) {
        super(name, hp*2);
    }

    public Monster (String name, int hp, String roar) {
        super(name, hp*2);
        this.roar = roar;
    }

    @Override
    public boolean canEat(Player target) {
        return super.canEat(target);
    }

    @Override
    public String getTipe() {
        return this.tipe = "Monster";
    }

    public String roar() {
        return roar;
    }
}

//  write Monster Class here

