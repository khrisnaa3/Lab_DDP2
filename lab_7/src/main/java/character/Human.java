package character;


public class Human extends Player {
    public Human (String name, int hp) {
        super(name, hp);
    }

    @Override
    public boolean canEat(Player target) {
        if (!(target instanceof Human) && target.matang) {
            return true;
        }
        return false;
    }

    @Override
    public String getTipe() {
        return this.tipe = "Human";
    }
}

//  write Human Class here

