/**
 * BingoCard Template created by Nathaniel Nicholas
 * Template untuk mengerjakan soal bonus tutorial lab 5
 * Template ini tidak wajib digunakan
 * Side Note : Jangan lupa untuk membuat class baru yang memiliki method main untuk menjalankan program dengan spesifikasi yang diharapkan
 */

public class BingoCard {

	private Number[][] numbers;
	private boolean isBingo;
	
	public BingoCard(Number[][] numbers) {
		this.numbers = numbers;
		this.isBingo = false;
	}

	public Number[][] getNumbers() {
		return numbers;
	}

	public void setNumbers(Number[][] numbers) {
		this.numbers = numbers;
	}

	public boolean isBingo() {
		return isBingo;
	}

	public void setBingo(boolean isBingo) {
		this.isBingo = isBingo;
	}

	public String markNum(int num) {
		
		for (int row = 0; row < numbers.length; row++) {
			for (int column = 0; column < numbers[row].length; column++) {
				if (numbers[row][column].getValue() == num) {
					if (numbers[row][column].isChecked()) {
						return (String.format("%d sebelumnya sudah tersilang", numbers[row][column].getValue()));
					}
					else {
						numbers[row][column].setChecked(true);
						cekBingo();
						return (String.format("%d tersilang", numbers[row][column].getValue()));
					}
				}
			}
		}
		return (String.format("Kartu tidak memiliki angka %d", num));
	}
	
	public String info () {
		String papan = "";

		for (int row = 0; row < numbers.length; row++) {
			for (int column = 0; column < numbers[row].length; column++) {
				papan += "| ";
				if (numbers[row][column].isChecked()) {
					papan += "X  ";
				}
				else {
					papan += (numbers[row][column].getValue() + " ");
				}
			}
			papan += ("|\n");
		}
		return papan.substring(0, papan.length() - 1 );
	}
	
	public void restart() {
		for (int row = 0; row < numbers.length; row++) {
			for (int column = 0; column < numbers[row].length; column++){
				numbers[row][column].setChecked(false);
				isBingo =false;
			}
		}
		System.out.println("Mulligan!");
	}

	public void cekBingo() {
		for (int row = 0; row < numbers.length; row++) {
			for (int column = 0; column < numbers[row].length; column++) {
			// Bingo Vertical
				if (numbers[0][column].isChecked() && numbers[1][column].isChecked() && numbers[2][column].isChecked() && numbers[3][column].isChecked() && numbers[4][column].isChecked()) {
				isBingo = true;
				}
			// Bingo Horizontal
			else if (numbers[row][0].isChecked() && numbers[row][1].isChecked() && numbers[row][2].isChecked() && numbers[row][3].isChecked() && numbers[row][4].isChecked()) {
				isBingo = true;
				}
			// Bingo Diagonal
			else if (numbers[0][0].isChecked() && numbers[1][1].isChecked() && numbers[2][2].isChecked() && numbers[3][3].isChecked() && numbers[4][4].isChecked()) {
				isBingo = true;
				}
			// Bingo Diagonal
			else if (numbers[0][4].isChecked() && numbers[1][3].isChecked() && numbers[2][2].isChecked() && numbers[3][1].isChecked() && numbers[4][0].isChecked()) {
				isBingo = true;
				}			
			}
		}
		if (isBingo) {
			System.out.println("Bingo !");
			System.out.println(info());
		}
	}
}