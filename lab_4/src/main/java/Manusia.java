public class Manusia{
	private String nama;
	private int umur, uang = 50000;
	private double kebahagiaan = 50;

	public Manusia(String nama, int umur){
		this.nama = nama;
		this.umur = umur;
	}

	public Manusia(String nama, int umur, int uang){
		this.nama = nama;
		this.umur = umur;
		this.uang = uang;
	}

	//manggil variable
	public String getNama(){
		return nama;
	}
	public int getUmur(){
		return umur;
	}
	public int getUang(){
		return uang;
	}
	public double getKebahagiaan(){
		return kebahagiaan;
	}

	//nge set isi variable
	public void setKebahagiaan(int kebahagiaan){
		this.kebahagiaan = (double)kebahagiaan;
	}
	public void setNama(String nama){
		this.nama = nama;
	}
	public void setUmur(int umur){
		this.umur = umur;
	}
	public void setUang(int uang){
		this.uang = uang;
	}

	//beri uang tanpa jumlah
	public void beriUang(Manusia penerima){
		int jumlahUang = 0;
		for (char i : penerima.nama.toCharArray()){
			jumlahUang += (int)i;
		}
		jumlahUang *= 100;
		if (jumlahUang > uang){
			System.out.println(String.format("%s ingin memberi uang kepada %s namun tidak memiliki cukup uang :'(", nama, penerima.nama));
		}else{
			penerima.kebahagiaan += ((double)jumlahUang/6000);
			kebahagiaan += ((double)jumlahUang/6000);
			penerima.uang += jumlahUang;
			uang -= jumlahUang;
			System.out.println(String.format("%s memberi uang sebanyak %d kepada %s, mereka berdua senang :D", nama, jumlahUang, penerima.nama));
		}
		if (kebahagiaan >= 100){
			kebahagiaan = 100;
		}
		if (penerima.kebahagiaan >= 100){
			penerima.kebahagiaan = 100;
		}
	}

	//beri uang dengan jumlah
	public void beriUang(Manusia penerima, int jumlahUang){
		if (jumlahUang > uang){
			System.out.println(String.format("%s ingin memberi uang kepada %s namun tidak memiliki cukup uang :'(", nama, penerima.nama));
		}else{
			penerima.kebahagiaan += ((double)jumlahUang/6000);
			kebahagiaan += ((double)jumlahUang/6000);
			penerima.uang += jumlahUang;
			uang -= jumlahUang;
			System.out.println(String.format("%s memberi uang sebanyak %d kepada %s, mereka berdua senang :D", nama, jumlahUang, penerima.nama));;
		}
		if (kebahagiaan >= 100){
			kebahagiaan = 100;
		}
		if (penerima.kebahagiaan >= 100){
			penerima.kebahagiaan = 100;
		}
	}

	//bekerja
	public void bekerja(int durasi, int bebanKerja){
		int Pendapatan = 0;
		if (umur <= 18){
			System.out.println(String.format("%s belum boleh bekerja karena masih dibawah umur D:", nama));
		}else{
			int BebanKerjaTotal = durasi*bebanKerja;
			if (BebanKerjaTotal <= kebahagiaan){
				kebahagiaan -= BebanKerjaTotal;
				Pendapatan = BebanKerjaTotal*10000;
				uang += Pendapatan;
				System.out.println(String.format("%s bekerja full time, total pendapatan : %d", nama, Pendapatan));
			}else{
				int DurasiBaru = (int)kebahagiaan/bebanKerja;
				BebanKerjaTotal = DurasiBaru*bebanKerja;
				kebahagiaan -= BebanKerjaTotal;
				Pendapatan = (int)BebanKerjaTotal*10000;
				uang += Pendapatan;
				System.out.println(String.format("%s tidak bekerja secara full time karena sudah terlalu lelah, total pendapatan : %d", nama, Pendapatan));
			}
		}
		if (kebahagiaan <= 0){
			kebahagiaan = 0;
		}
	}

	//rekreasi
	public void rekreasi(String namaTempat){
		int Biaya = namaTempat.length()*10000;
		if (Biaya <= uang){
			uang -= Biaya;
			kebahagiaan += namaTempat.length();
			System.out.println(String.format("%s berekreasi di %s, %s senang :)", nama, namaTempat, nama));
		}else{
			System.out.println(String.format("%s tidak mempunyai cukup uang untuk berekreasi di %s :(", nama, namaTempat));
		}
		if (kebahagiaan >= 100){
			kebahagiaan = 100;
		}
	}

	//sakit
	public void sakit(String penyakit){
		kebahagiaan -= penyakit.length();
		System.out.println(String.format("%s terkena penyakit %s :O", nama, penyakit));
		if (kebahagiaan <= 0){
			kebahagiaan = 0;
		}
	}

	//toString
	public String toString(){
		return String.format("Nama\t\t: %s \nUmur\t\t: %d \nUang\t\t: %d \nKebahagiaan\t: %f", nama, umur, uang, kebahagiaan);
	}
}