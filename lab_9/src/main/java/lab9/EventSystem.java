package lab9;

import lab9.user.User;
import lab9.event.Event;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Class representing event managing system
 * @author Khrisna
 */
public class EventSystem {
    private SimpleDateFormat jadwal = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
    private Date startTime, endTime;

    /**
     * List of events
     */
    private ArrayList<Event> events;

    /**
     * List of users
     */
    private ArrayList<User> users;

    /**
     * Constructor. Initializes events and users with empty lists.
     */
    public EventSystem() {
        this.events = new ArrayList<>();
        this.users = new ArrayList<>();
    }

    /**
     * Menambah event baru
     *
     * @param name
     * @param startTimeStr
     * @param endTimeStr
     * @param costPerHourStr
     * @return String dengan format sesuai case
     * Jika valid, maka akan dibuat object Event baru
     */
    public String addEvent(String name, String startTimeStr, String endTimeStr, String costPerHourStr) {
        // mengecek ketersediaan event
        if (getEvent(name) != null) {
            return "Event " + name + " sudah ada!";
        }

        // membuat objek date
        try {
            startTime = jadwal.parse(startTimeStr);
            endTime = jadwal.parse(endTimeStr);
        } catch (Exception e) {
            System.out.println(e);
        }

        if (startTime.after(endTime) || startTime.equals(endTime)) {
            return "Waktu yang diinputkan tidak valid!";
        }
        events.add(new Event(name, startTime, endTime, costPerHourStr));
        return "Event " + name + " berhasil ditambahkan!";
    }

    /**
     * @param name
     * @return String dengan format sesuai case
     * Jika tidak ada user dengan nama yang sama, akan dibuat object User baru
     */
    public String addUser(String name) {
        if (getUser(name) != null) {
            return "User " + name + " sudah ada!";
        }
        users.add(new User(name));
        return "User " + name + " berhasil ditambahkan!";

    }

    /**
     * @param userName
     * @param eventName
     * @return String dengan format sesuai case
     */
    public String registerToEvent(String userName, String eventName) {
        // Jika tidak ada event dengan suatu nama event
        if (getEvent(eventName) == null) {
            return "Tidak ada acara dengan nama " + eventName + "!";
        }
        // Jika tidak ada user dengan suatu nama user
        else if (getUser(userName) == null) {
            return "Tidak ada pengguna dengan nama " + userName + "!";
        }
        // Jika tidak ada event dan user
        else if (getUser(userName) == null && getEvent(eventName) == null) {
            return "Tidak ada pengguna dengan nama " + userName + " dan acara dengan nama " + eventName + "!";
        }
        // Jika ada event bertabrakan
        else if (getUser(userName).eventChecker(getEvent(eventName))) {
            return userName + " sibuk sehingga tidak dapat menghadiri " + eventName + "!";
        }
        getUser(userName).addEvent(getEvent(eventName));
        return userName + " berencana menghadiri " + eventName + "!";
    }

    /**
     * Mengambil object User tertentu sesuai nama user yang ditentukan
     *
     * @param userName
     * @return sebuah object User
     */
    public User getUser(String userName) {
        for (User user : users) {
            if (user.getName().equals(userName)) return user;
        }
        return null;
    }

    /**
     * Mengambil object Event tertentu sesuai nama event yang ditentukan
     *
     * @param eventName
     * @return sebuah object Event
     */
    public Event getEvent(String eventName) {
        for (Event event : events) {
            if (event.getName().equals(eventName)) return event;
        }
        return null;
    }
}