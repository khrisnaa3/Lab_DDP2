package lab9.user;

import lab9.event.Event;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Class representing a user, willing to attend event(s)
 * @author Khrisna
 */
public class User {
    /**
     * Name of user
     */
    private String name;

    /**
     * List of events this user plans to attend
     */
    private ArrayList<Event> events;

    private BigInteger totalCost = new BigInteger("0");

    /**
     * Constructor
     * Initializes a user object with given name and empty event list
     */
    public User(String name) {
        this.name = name;
        this.events = new ArrayList<>();
    }

    /**
     * Accessor for name field
     *
     * @return name of this instance
     */
    public String getName() {
        return name;
    }

    /**
     * Adds a new event to this user's planned events, if not overlapping
     * with currently planned events.
     *
     * @return true if the event if successfully added, false otherwise
     */
    public boolean addEvent(Event newEvent) {
        if (!eventChecker(newEvent)) {
            totalCost = totalCost.add(newEvent.getCost());
            events.add(newEvent);
            return true;
        }
        return false;
    }

    /**
     * Returns the list of events this user plans to attend,
     * Sorted by their starting time.
     * Note: The list returned from this method is a copy of the actual
     * events field, to avoid mutation from external sources
     *
     * @return list of events this user plans to attend
     */
    public ArrayList<Event> getEvents() {
        // TODO: Implement!
        // WARNING: The list returned needs to be a copy of the actual events list.
        //          You don't want people to change your plans (e.g. clearing the
        //          list) without your consent, right?
        // HINT: see Java API Documentation on ArrayList (java.util.ArrayList)
        //                                                                                                     (or... Google. Yeah that works too. OK.)
        ArrayList<Event> eventsClone = (ArrayList<Event>) events.clone();
        Collections.sort(eventsClone, new Comparator<Event>() {
            @Override
            public int compare(Event o1, Event o2) {
                return o1.getStartTime().compareTo(o2.getStartTime());
            }
        });
        return eventsClone;
    }

    /**
     * Mengambil total cost
     *
     * @return totalCost
     */
    public BigInteger getTotalCost() {
        return totalCost;
    }

    /**
     * Mengecek jadwal antara event baru dan event lama
     *
     * @param event
     * @return FALSE jika tidak ada event yang bertabrakan
     */
    public boolean eventChecker(Event event) {
        for (Event eachEvent : events) {
            // Mengecek jadwal lama berada di antara jadwal baru
            if (event.getStartTime().before(eachEvent.getStartTime()) && event.getEndTime().after(eachEvent.getEndTime())) {
                return true;
            }
            // Mengecek endTime event baru berada di tengah2 jadwal event lama
            else if (event.getStartTime().after(eachEvent.getStartTime()) && event.getStartTime().before(eachEvent.getEndTime())) {
                return true;
            }
            // Mengecek startTime event baru berada di tengah2 jadwal event lama
            else if (event.getEndTime().after(eachEvent.getStartTime()) && event.getEndTime().before(eachEvent.getEndTime())) {
                return true;
            }
        }
        return false;
    }
}
