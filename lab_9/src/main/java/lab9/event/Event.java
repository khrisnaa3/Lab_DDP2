package lab9.event;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * A class representing an event and its properties
 * @author Khrisna
 */
public class Event {
    /**
     * Name of event
     */
    private String name;
    private Date startTime, endTime;
    private BigInteger cost;
    private GregorianCalendar jadwalStart = new GregorianCalendar();
    private GregorianCalendar jadwalEnd = new GregorianCalendar();
    private SimpleDateFormat jadwal = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
    private SimpleDateFormat printJadwal = new SimpleDateFormat("dd-MM-yyy, HH:mm:ss");

    public Event(String name, Date startTime, Date endTime, String costPerHourStr) {
        this.name = name;
        this.cost = new BigInteger(costPerHourStr);
        this.startTime = startTime;
        this.endTime = endTime;
        jadwalStart.setTime(startTime);
        jadwalEnd.setTime(startTime);
    }

    // TODO: Make instance variables for representing beginning and end time of event

    // TODO: Make instance variable for cost per hour

    // TODO: Create constructor for Event class

    /**
     * Accessor for name field.
     *
     * @return name of this event instance
     */
    public String getName() {
        return this.name;
    }

    /**
     * Accessor for cost
     *
     * @return cost of this event instance
     */
    public BigInteger getCost() {
        return cost;
    }

    /**
     * Accessor for endTime
     *
     * @return endTime of this event instance
     */
    public Date getEndTime() {
        return endTime;
    }

    /**
     * Accessor for startTime
     *
     * @return startTime of this event instance
     */
    public Date getStartTime() {
        return startTime;
    }

    // TODO: Implement toString()

    @Override
    public String toString() {
        return name + "\n"
                + "Waktu mulai: " + printJadwal.format(startTime) + "\n"
                + "Waktu selesai: " + printJadwal.format(endTime) + "\n"
                + "Biaya kehadiran: " + cost;
    }
    // HINT: Implement a method to test if this event overlaps with another event
    //       (e.g. boolean overlapsWith(Event other)). This may (or may not) help
    //       with other parts of the implementation.
}
