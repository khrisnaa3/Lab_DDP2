import java.util.Scanner;

public class BingoMain {
	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);
		
		Number[][] numbers = new Number[5][5];

		for (int a = 0; a < 5; a++) {
			for (int b = 0; b < 5; b++) {
				numbers[a][b] = new Number(input.nextInt(),a,b);
			}
		}

		BingoCard card = new BingoCard(numbers);

		while (true) {
			String user = input.nextLine();
			String[] userInput = user.split(" ");
			if (userInput[0].equals("MARK")) {
				System.out.println(card.markNum(Integer.valueOf(userInput[1])));
			}
			else if (userInput[0].equals("INFO")) {
				System.out.println(card.info());
			}
			else if (userInput[0].equals("RESTART")) {
				card.restart();
			}
		}
	}
}