import java.util.Scanner;

/**
 * @author Template Author: Ichlasul Affan dan Arga Ghulam Ahmad
 * Template ini digunakan untuk Tutorial 02 DDP2 Semester Genap 2017/2018.
 * Anda sangat disarankan untuk menggunakan template ini.
 * Namun Anda diperbolehkan untuk menambahkan hal lain berdasarkan kreativitas Anda
 * selama tidak bertentangan dengan ketentuan soal.
 *
 * Cara penggunaan template ini adalah:
 * 1. Isi bagian kosong yang ditandai dengan komentar dengan kata TODO
 * 2. Ganti titik-titik yang ada pada template agar program dapat berjalan dengan baik.
 *
 * Code Author (Mahasiswa):
 * @author Stefanus Khrisna A.H, NPM 1706074921, Kelas DDP2-F, GitLab Account: khrisnaa3
 */

public class SistemSensus {
	public static void main(String[] args) {
		// Buat input scanner baru
		Scanner input = new Scanner(System.in);


		// TODO Bagian ini digunakan untuk soal Tutorial "Sensus Daerah Kumuh"
		// User Interface untuk meminta masukan
		System.out.print("PROGRAM PENCETAK DATA SENSUS\n" +
				"--------------------\n" +
				"Nama Kepala Keluarga   : ");
		String nama = input.nextLine();
		System.out.print("Alamat Rumah           : ");
		String alamat = input.nextLine();
		System.out.print("Panjang Tubuh (cm)     : ");
		double panjang = Double.parseDouble(input.nextLine());
		System.out.print("Lebar Tubuh (cm)       : ");
		double lebar = Double.parseDouble(input.nextLine());
		System.out.print("Tinggi Tubuh (cm)      : ");
		double tinggi = Double.parseDouble(input.nextLine());
		System.out.print("Berat Tubuh (kg)       : ");
		double berat = Double.parseDouble(input.nextLine());
		System.out.print("Jumlah Anggota Keluarga: ");
		int makanan = input.nextInt();
		input.nextLine();
		System.out.print("Tanggal Lahir          : ");
		String tanggalLahir = input.nextLine();
		System.out.print("Catatan Tambahan       : ");
		String catatan = input.nextLine();
		System.out.print("Jumlah Cetakan Data    : ");
		int jumlahCetakan = input.nextInt();
		input.nextLine();


		// TODO Bagian ini digunakan untuk soal Tutorial "Sensus Daerah Kumuh"
		// TODO Hitung rasio berat per volume (rumus lihat soal)
		double rasio = (berat) / ((panjang/100) * (lebar/100) * (tinggi/100));
		int rasio_int = (int) rasio;

		for (int i = 1; i <= jumlahCetakan; i++) {
			// TODO Minta masukan terkait nama penerima hasil cetak data
			System.out.print("Pencetakan " + i + " dari " + jumlahCetakan + " untuk: ");
			String penerima = input.nextLine().toUpperCase(); // Lakukan baca input lalu langsung jadikan uppercase

			// TODO Periksa ada catatan atau tidak
			if (catatan.length() > 0) {
				catatan = "Catatan: " + catatan;
			}else {
				catatan = "Tidak ada catatan tambahan";
			}

			// TODO Cetak hasil (ganti string kosong agar keluaran sesuai)
			String hasil = "";
			System.out.format("DATA SIAP DICETAK UNTUK %s \n---------------------- \n", penerima);
			System.out.println(String.format("%s - %s", nama, alamat));
			System.out.println(String.format("Lahir Pada Tanggal %s", tanggalLahir));
			System.out.println(String.format("Rasio Berat Per Volume = %d kg/m^3", rasio_int));
			System.out.println(catatan + "\n");
		}


//		// TODO Bagian ini digunakan untuk soal bonus "Rekomendasi Apartemen"
//		// TODO Hitung nomor keluarga dari parameter yang telah disediakan (rumus lihat soal)
//
//
//
//		// TODO Gabungkan hasil perhitungan sesuai format sehingga membentuk nomor keluarga
//		String nomorKeluarga = "";
//
//		// TODO Hitung anggaran makanan per tahun (rumus lihat soal)
//		... anggaran = (...) (...);
//
//		// TODO Hitung umur dari tanggalLahir (rumus lihat soal)
//		... tahunLahir = .....; // lihat hint jika bingung
//		... umur = (...) (...);
//
//		// TODO Lakukan proses menentukan apartemen (kriteria lihat soal)
//
//
//
//
//
//		// TODO Cetak rekomendasi (ganti string kosong agar keluaran sesuai)
//		String rekomendasi = "";
//		.....;

//		input.close();
	}
}