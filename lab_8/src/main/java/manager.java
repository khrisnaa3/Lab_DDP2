public class manager extends karyawan{
    public manager(String nama, String jabatan, int gaji) {
        this.nama = nama;
        this.jabatan = jabatan;
        this.gaji = gaji;
    }

    @Override
    public void bawahan(String namaBawahan, String jabatanBawahan) {
        if (jabatanBawahan.equalsIgnoreCase("manager")) {
            System.out.println("Anda tidak layak memiliki bawahan");
        } else {
            this.listBawahan.add(namaBawahan);
            System.out.printf("Karyawan %s berhasil ditambahkan menjadi bawahan %s\n", namaBawahan, getNama());
        }
    }
}
