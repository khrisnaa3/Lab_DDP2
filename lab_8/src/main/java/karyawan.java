import java.util.ArrayList;

public abstract class karyawan {
    protected ArrayList<String> listBawahan = new ArrayList<>();
    protected String nama, jabatan;
    protected int tabungan, counterGajian, gaji;

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getJabatan() {
        return jabatan;
    }

    public void setJabatan(String jabatan) {
        this.jabatan = jabatan;
    }

    public int getGaji() {
        return gaji;
    }

    public void setGaji(int gaji) {
        this.gaji = gaji;
    }

    public int getCounterGajian() {
        return counterGajian;
    }

    public void setCounterGajian(int counterGajian) {
        this.counterGajian = counterGajian;
    }

    public int getTabungan() {
        return tabungan;
    }

    public void gajian() {
        this.tabungan += this.gaji;
        int gajiAwal = this.gaji;
        this.counterGajian += 1;
        if (this.counterGajian % 6 == 0) {
            this.gaji += this.gaji/10;
            System.out.println(getNama() + " mengalami kenaikan gaji sebesar 10% dari " + gajiAwal + " menjadi " + getGaji());
        }
    }
    public abstract void bawahan(String namaBawahan, String jabatanBawahan);
}
