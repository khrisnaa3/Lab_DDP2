import java.util.ArrayList;
import java.util.Scanner;

class Lab8 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        ArrayList<karyawan> karyawans = new ArrayList<>();
        int batasGaji = Integer.parseInt(input.nextLine());
        while (true) {
            String userInput = input.nextLine();
            String[] userSplit = userInput.split(" ");
            if (userSplit[0].equalsIgnoreCase("TAMBAH_KARYAWAN")) {
                String jabatan = userSplit[2];
                String nama = userSplit[1];
                karyawan cekKaryawan = cariKaryawan(nama, karyawans);
                if (cekKaryawan != null) {
                    System.out.println("Karyawan dengan nama " + nama + " telah terdaftar");
                }
                else {
                    String output = nama + " mulai bekerja sebagai " + jabatan + " di PT. TAMPAN";
                    int gaji = Integer.parseInt(userSplit[3]);
                    if (jabatan.equalsIgnoreCase("MANAGER")) {
                        karyawans.add(new manager(nama, jabatan, gaji));
                        System.out.println(output);
                    }
                    else if (jabatan.equalsIgnoreCase("STAFF")) {
                        karyawans.add(new staff(nama, jabatan, gaji));
                        System.out.println(output);
                    }
                    else if (jabatan.equalsIgnoreCase("INTERN")) {
                        karyawans.add(new intern(nama, jabatan, gaji));
                        System.out.println(output);
                    }
                }
            }
            if (userSplit[0].equalsIgnoreCase("STATUS")) {
                String nama = userSplit[1];
                karyawan cekKaryawan = cariKaryawan(nama, karyawans);
                if (cekKaryawan == null) {
                    System.out.println("Karyawan tidak ditermukan");
                } else {
                    System.out.println(nama + " " + cekKaryawan.getGaji());
                }
            }

            if (userSplit[0].equalsIgnoreCase("TAMBAH_BAWAHAN")) {
                String namaAtasan = userSplit[1];
                String namaBawahan = userSplit[2];
                karyawan cekKaryawanBawahan = cariKaryawan(namaBawahan, karyawans);
                karyawan cekKaryawanAtasan = cariKaryawan(namaAtasan, karyawans);

                if (cekKaryawanAtasan == null || cekKaryawanBawahan == null) {
                    System.out.println("Nama tidak berhasil ditemukan");
                } else {
                    String cekJabatanBawahan = cekKaryawanBawahan.getJabatan();
                    cekKaryawanAtasan.bawahan(namaBawahan, cekJabatanBawahan);
                }
            }

            if (userSplit[0].equalsIgnoreCase("GAJIAN")) {
                for (karyawan i : karyawans) {
                    i.gajian();
                    if (i.getGaji() > batasGaji && i instanceof staff) {
                        System.out.println("Selamat, " + i.getNama() + " telah dipromosikan menjadi MANAGER");
                        manager promosi = new manager(i.getNama(), "manager", i.getGaji());

                        karyawans.set(karyawans.indexOf(i), promosi);
                    }
                }
                System.out.println("Semua karyawan telah diberikan gaji");
            }
        }
    }

    public static karyawan cariKaryawan(String nama, ArrayList<karyawan> karyawans) {
        for (int i = 0; i < karyawans.size(); i++){
            karyawan objKaryawan = karyawans.get(i);
            if (nama.equalsIgnoreCase(objKaryawan.getNama())) {
                return objKaryawan;
            }
        }
        return null;
    }
}