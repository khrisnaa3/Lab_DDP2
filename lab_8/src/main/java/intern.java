public class intern extends karyawan {
    public intern(String nama, String jabatan, int gaji) {
        this.nama = nama;
        this.jabatan = jabatan;
        this.gaji = gaji;
    }

    @Override
    public void bawahan(String namaBawahan, String jabatanBawahan) {
        System.out.println("Anda tidak layak memiliki bawahan");
    }
}
