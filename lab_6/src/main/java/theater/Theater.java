package theater;

import movie.Movie;
import ticket.Ticket;

import java.util.ArrayList;

public class Theater {
    private String namaBioskop;
    private int saldo;
    private ArrayList<Ticket> ticket;
    private Movie[] film;
    private int jumlahTiket;
    private static int totalRevenue;

    public Theater(String namaBioskop, int saldo, ArrayList<Ticket> ticket, Movie[] film) {
        this.namaBioskop = namaBioskop;
        this.saldo = saldo;
        totalRevenue += saldo;
        this.film = film;
        this.ticket = ticket;
        this.jumlahTiket = ticket.size();
    }

    public String getNamaBioskop() {
        return namaBioskop;
    }

    public Movie[] getFilm() {
        return film;
    }

    public static void orderTicket(int uang) {
        totalRevenue += uang;
    }

    public void setSaldo(int saldo) {
        this.saldo += saldo;
    }

    public void printInfo() {
        String daftarFilm = "";
        for (Movie setiapFilm : film) {
            daftarFilm += setiapFilm.getJudul() + ", ";
        }

        System.out.printf("------------------------------------------------------------------\n" +
                "Bioskop                : %s\n" +
                "Saldo Kas              : %s\n" +
                "Jumlah tiket tersedia  : %d\n" +
                "Daftar Film tersedia   : %s\n" +
                "------------------------------------------------------------------\n", namaBioskop, saldo, jumlahTiket, daftarFilm);
    }

    public Ticket cekTiket(String judul, String hari, String jenis) {
        boolean dimension3 = false;
        if (jenis.equals("3 Dimensi")) {
            dimension3 = true;
        }
        for (Ticket tiket : ticket) {
            if (tiket.getFilm().getJudul().equals(judul) && tiket.getHari().equals(hari) && dimension3 == tiket.isJenis()) {
                return tiket;
            }
        }
        return null;
    }

    public void printRevenue() {
        System.out.printf("Bioskop      : %s\n", namaBioskop);
        System.out.printf("Saldo Kas    : %d\n", saldo);
        System.out.println("\n------------------------------------------------------------------\n");
    }

    public static void printTotalRevenueEarned(Theater[] bioskop) {
        System.out.println("Total uang yang dimiliki Koh Mas : Rp. " + totalRevenue);
        System.out.println("------------------------------------------------------------------");
        for (Theater i : bioskop) {
            i.printRevenue();
        }
    }

    public boolean cekUmur(String judul, int umur) {
        for (Movie setiapFilm : film) {
            if (setiapFilm.getJudul().equals(judul) && setiapFilm.getRatingUmur().equals("Umum")) {
                return true;
            } else if (setiapFilm.getJudul().equals(judul) && setiapFilm.getRatingUmur().equals("Remaja") && umur >= 13) {
                return true;
            } else if (setiapFilm.getJudul().equals(judul) && setiapFilm.getRatingUmur().equals("Dewasa") && umur >= 17) {
                return true;
            }
        }
        return false;
    }

    public Movie findFilm(String judul) {
        for (Movie i : film) {
            if (i.getJudul().equals(judul)) {
                return i;
            }
        }
        return null;
    }
}
