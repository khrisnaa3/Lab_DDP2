package movie;


public class Movie {
    private String judul;
    private String ratingUmur;
    private int durasi;
    private String genreFilm;
    private String status;

    public Movie(String judul, String ratingUmur, int durasi, String genreFilm, String status) {
        this.judul = judul;
        this.ratingUmur = ratingUmur;
        this.durasi = durasi;
        this.genreFilm = genreFilm;
        this.status = status;
    }

    public String getJudul() {
        return judul;
    }

    public String getRatingUmur() {
        return ratingUmur;
    }

    public int getDurasi() {
        return durasi;
    }

    public String getGenreFilm() {
        return genreFilm;
    }

    public String getStatus() {
        return status;
    }

    public void infoMovie() {
        System.out.printf("------------------------------------------------------------------\n" +
                "Judul  : %s\n" +
                "Genre  : %s\n" +
                "Durasi : %s menit\n" +
                "Rating : %s\n" +
                "Jenis  : Film %s\n" +
                "------------------------------------------------------------------\n", getJudul(), getGenreFilm(), getDurasi(), getRatingUmur(), getStatus());
    }
}
