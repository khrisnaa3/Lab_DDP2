package ticket;

import movie.Movie;

public class Ticket {

    private String hari;
    private Movie film;
    private boolean jenis;
    private int hargaTiket = 60000;
    private int jumlahTiket;

    public Ticket(Movie film, String hari, boolean jenis) {
        this.film = film;
        this.hari = hari;
        this.jenis = jenis;
        jumlahTiket++;
        if (hari.equals("Sabtu") || hari.equals("Minggu")) {
            hargaTiket += 40000;
        }
        if (isJenis()) {
            hargaTiket += hargaTiket * 20 / 100;
        }
    }

    public String getHari() {
        return hari;
    }

    public Movie getFilm() {
        return film;
    }

    public boolean isJenis() {
        return jenis;
    }

    public String getJenis() {
        if (isJenis()) {
            return "3 Dimensi";
        }
        return "Biasa";
    }

    public int getHargaTiket() {
        return hargaTiket;
    }

    public String infoTicket() {
        String hasil = String.format("------------------------------------------------------------------\n" +
                "Film           : %s\n" +
                "Jadwal Tayang  : %s\n" +
                "Jenis          : %s\n" +
                "------------------------------------------------------------------\n", getFilm(), getHari(), getJenis());
        return hasil;
    }
}
