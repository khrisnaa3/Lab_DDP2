package customer;

import movie.Movie;
import theater.Theater;
import ticket.Ticket;

public class Customer {

    private String namaPelanggan;
    private String jenisKelamin;
    private int umur;

    public Customer(String namaPelanggan, String jenisKelamin, int umur) {
        this.namaPelanggan = namaPelanggan;
        this.jenisKelamin = jenisKelamin;
        this.umur = umur;
    }

    public void findMovie(Theater bioskop, String judul) {
        for (Movie satuFilm : bioskop.getFilm()) {
            if (satuFilm.getJudul().equals(judul)) {
                satuFilm.infoMovie();
                return;
            }
        }
        System.out.printf("Film %s yang dicari %s tidak ada di bioskop %s\n", judul, namaPelanggan, bioskop.getNamaBioskop());
    }

    public Ticket orderTicket(Theater bioskop, String judul, String hari, String jenis) {
        if (bioskop.cekTiket(judul, hari, jenis) == null) {
            System.out.printf("Tiket untuk film %s jenis %s dengan jadwal %s tidak tersedia di %s\n",
                    judul, jenis, hari, bioskop.getNamaBioskop());
            return null;
        }
        if (!bioskop.cekUmur(judul, umur)) {
            System.out.printf("%s masih belum cukup umur untuk menonton %s dengan rating %s\n",
                    namaPelanggan, judul, bioskop.findFilm(judul).getRatingUmur());
            return null;
        }

        Theater.orderTicket(bioskop.cekTiket(judul, hari, jenis).getHargaTiket());
        bioskop.setSaldo(bioskop.cekTiket(judul, hari, jenis).getHargaTiket());
        System.out.printf("%s telah membeli tiket %s jenis %s di %s pada hari %s seharga Rp. %d\n",
                namaPelanggan, judul, jenis, bioskop.getNamaBioskop(), hari, bioskop.cekTiket(judul, hari, jenis).getHargaTiket());
        return bioskop.cekTiket(judul, hari, jenis);

    }
}
